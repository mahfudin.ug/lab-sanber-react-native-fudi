/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import firebase from '@react-native-firebase/app'
import AppNavigation from './navigation';

const firebaseConfig = {
  apiKey: "AIzaSyCzJ_YkeI18SxkfKAGjo-RosywcGWqPBDA",
  authDomain: "lab-sanber-rna.firebaseapp.com",
  databaseURL: "https://lab-sanber-rna.firebaseio.com",
  projectId: "lab-sanber-rna",
  storageBucket: "lab-sanber-rna.appspot.com",
  messagingSenderId: "546285868427",
  appId: "1:546285868427:web:2e349c7ea94b05749f8b8e"
};

// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App: () => React$Node = () => {
  return (
    <>
      <AppNavigation />
    </>
  );
};

export default App;
