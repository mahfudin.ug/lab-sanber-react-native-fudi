export default {
  base: "https://crowdfunding.sanberdev.com",
  api: "https://crowdfunding.sanberdev.com/api",
}
