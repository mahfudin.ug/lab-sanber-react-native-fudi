import React, { useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import SplashScreen from '../screens/SplashScreen'
import Intro from '../screens/Intro';
import Login from '../screens/Login'
import Profile from '../screens/Profile';
import ProfileEdit from '../screens/ProfileEdit';
import Register from '../screens/Register';
import Home from '../screens/Home';
import Donation from '../screens/Donation';
import DonationDetail from '../screens/DonationDetail';
import Payment from '../screens/Payment';
import Colors from '../styles/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Help from '../screens/Help';
import Statistic from '../screens/Statistic';
import Inbox from '../screens/Inbox';

const Stack = createStackNavigator()
const Tab = createMaterialBottomTabNavigator()

const ProfileNavigation = () => {
  return (
    <Stack.Navigator initialRouteName="Profile">
      <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
      <Stack.Screen name="ProfileEdit" component={ProfileEdit} options={{ headerShown: true }} />
      <Stack.Screen name="Help" component={Help} options={{ headerShown: true }} />
    </Stack.Navigator>
  )
}

const HomeNavigation = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
      <Stack.Screen name="Donation" component={Donation} options={{ headerShown: true }} />
      <Stack.Screen name="Payment" component={Payment} options={{ headerShown: true }} />
      <Stack.Screen name="DonationDetail" component={DonationDetail} 
        options={{ 
          headerShown: true, 
          headerTransparent: true, 
          title: '', 
          headerTintColor: 'white' 
        }} 
      />
      <Stack.Screen name="Statistic" component={Statistic} options={{ headerShown: true }} />
    </Stack.Navigator>
  )
}

const DashboardTab = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomeTab"
      activeColor={Colors.red}
      labelStyle={{ fontSize: 12 }}
    >
      <Tab.Screen
        name="HomeTab"
        component={HomeNavigation}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home-outline" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Inbox"
        component={Inbox}
        options={{
          tabBarLabel: 'Inbox',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="chat-processing-outline" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="ProfileTab"
        component={ProfileNavigation}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account-outline" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  )
}

const MainNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
      <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
      <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
      <Stack.Screen name="DashboardTab" component={DashboardTab} options={{ headerShown: false }} />

    </Stack.Navigator>
  )
}

const AppNavigation = () => {

  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading)
    }, 3000)
  }, []) 

  if (isLoading) {
    return <SplashScreen />
  }

  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  )
}

export default AppNavigation
