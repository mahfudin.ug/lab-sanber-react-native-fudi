import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import colors from '../../styles/Colors';


const Register = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.textLogoContainer}>
        <Text style={styles.textLogo}>Register Screen</Text>
      </View>

      <View style={styles.btnContainer}>
        <TouchableOpacity onPress={() => navigation.navigate('Intro')} style={styles.btnBack} activeOpacity={0.7}>
          <Text style={styles.btnBackText}>Back to Intro</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Register

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.blue,
    justifyContent: 'space-between'
  },
  textLogoContainer: {
    marginTop: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLogo: {
    fontSize: 25,
    fontWeight: 'bold',
    color: colors.white
  },
  btnContainer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 15
  },
  btnBack: {
    height: 35,
    width: '90%',
    borderWidth: 1.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colors.white,
    backgroundColor: 'transparent',
  },
  btnBackText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.white
  }
}) 
