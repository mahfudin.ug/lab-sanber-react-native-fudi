import React from 'react'
import { StyleSheet, Text, View, SafeAreaView, StatusBar, Image } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider'
import colors from '../../styles/Colors'
import {Button} from '../../components/Button'

const data = [
  {
    id: 1,
    // image: require('../../assets/images/person-placeholder.jpg'),
    image: {uri: 'http://placeimg.com/480/480/nature'},
    description: 'Just imagine, this is crowd funding illustration.\n Which make you so interest',
  },
  {
    id: 2,
    image: {uri: 'http://placeimg.com/480/480/people'},
    description: 'Just imagine, this is crowd funding illustration.\n Which make you feel like helping'
  },
  {
    id: 3,
    image: {uri: 'http://placeimg.com/480/480/animals'},
    description: 'Just imagine, this is crowd funding illustration.\n Which make you sure to donate',
  },

]

const Intro = ({ navigation }) => {

  const renderItem = ({ item }) => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listContent}>
          <Image source={item.image} style={styles.imgList} resizeMethod="auto" resizeMode="contain" />
        </View>
        <Text style={styles.textList}>{item.description}</Text>
      </View>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.green} barStyle="light-content" />
        <View style={styles.textLogoContainer}>
          <Text style={styles.textLogo}>Crowd Funding</Text>
        </View>
        <View style={styles.slider}>
          <AppIntroSlider 
            data={data}
            renderItem={renderItem}
            renderNextButton={() => null}
            renderDoneButton={() => null}
            activeDotStyle={styles.activeDotStyle}
            keyExtractor={(item) => item.id.toString() }
          />
        </View>

        <View style={styles.btnContainer}>
          <Button style={styles.btnLogin} onPress={() => navigation.replace('Login')} >
            <Text style={styles.btnTextLogin}>LOGIN</Text>
          </Button>
          <Button style={styles.btnRegister} onPress={() => navigation.replace('Register')} ><
            Text style={styles.btnTextRegister}>REGISTER</Text>
          </Button>
        </View>

      </View>
    </SafeAreaView>
  )
}

export default Intro

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: colors.green
  },
  textLogoContainer: {
      marginTop: 25,
      alignItems: 'center',
      justifyContent: 'center'
  },
  textLogo: {
      fontSize: 24,
      fontWeight: 'bold',
      color: colors.white
  },
  slider: {
      flex: 1
  },
  btnContainer: {
      marginBottom: 15,
      alignItems: 'center',
      justifyContent: 'flex-end'
  },
  btnLogin: {
      height: 35,
      width: '90%',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: colors.white
  },
  btnTextLogin: {
      fontSize: 14,
      fontWeight: 'bold',
      color: colors.green
  },
  btnRegister: {
      height: 35,
      width: '90%',
      borderWidth: 1.5,
      alignItems: 'center',
      justifyContent: 'center',
      borderColor: colors.white,
      backgroundColor: 'transparent'
  },
  btnTextRegister: {
      fontSize: 14,
      fontWeight: 'bold',
      color: colors.white
  },
  listContainer: {
      marginTop: 25,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column'
  },
  listContent: {
      marginTop: 40,
      alignItems: 'center',
      justifyContent: 'center'
  },
  imgList: {
      width: 330,
      height: 330,
      borderRadius: 165
  },
  textList: {
      fontSize: 14,
      fontWeight: 'bold',
      color: colors.white,
      textAlign: 'center',
      marginTop: 15
  },
  activeDotStyle: {
      width: 20,
      backgroundColor: colors.white
  }
})
