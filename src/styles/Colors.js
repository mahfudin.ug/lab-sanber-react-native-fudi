export default {
  white : '#ffffff',
  blue: '#3a86ff',
  blue: '#3a86ff',
  violet: '#191970',
  black: '#000000',
  grey: '#A0A0A0',
  lightGrey: '#e6e6e6',
  green: '#1EBC61',
  red: '#e74c3c',
  transparent: 'rgba(0, 0, 0, 0.5)'
}